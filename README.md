## Test Automation using java Serenity, cucumber and  maven

Open API - Apacta test cases has been created for LeasePlan, as test assignment.

## Getting Started

### Testing Apacta Api

This project was done using Java Serenity, Cucumber BDD framework and Maven build tool.

### Installing

In order to execute API tests, it is necessary to configure CI/CD pipeline with docker maven image.

### How to build

#### Run below maven command to build the project.

$ mvn clean

### How to execute test cases [as below]

#### Run all the test cases.

$ mvn verify

### Write new test cases in a file with the path specified below

- src/test/resources/features  [for writing scenario's]
- src/test/java/com.apacta/steps  [for calling methods]
- src/test/java/com.apacta/actions [for writing methods]
- src/test/java/com.apacta/utils  [for writing util classes]
- src/test/java/com.apacta/models [for writing models]

### Serenity HTML Reports

[Report will be generated in target/site/serenity/index.html file]

## Summary - Features Coverability

- Validate retrieving and creating the time entries
- Validate creating time entries with invalid data got 404
- Validate retrieving city 
- Validate retrieving and creating the contacts
- Validate retrieving forms and single form with form id
- Validate retrieving and deleting invoices with authorised user
- Validate retrieving invoices with unauthorised user got 401
- Validate retrieving product

## Created CI/CD Pipeline and generated html report

- Committed code on gitlab using commands: git clone, git commit -m "commit message", git add <filename>, git status,
  git push, git pull
- Created gitlab-ci.yml file for generating CI/CD pipeline.
- CI/CD pipeline will get trigger automatically after the commit and have made two stages in pipeline : build and test (
  this stage will create public folder and moves the reports inside it)

### See serenity HTML reports

- Goto CI/CD - pipelines -> stage test (click) -> report-job (click) -> click Browse (right side) -> Public (click) ->
  search file index.html (click and we can see reports), else we can also download the reports and see (that will
  download public folder, so we can search index.html inside it)

## Use Authentication key
Created authentication key on portal https://app.apacta.com
api_key="2577a97e-bb4a-4ce4-8d39-da6d6c810ff9

## Interactions of endpoints

### Validate retrieving and posting time entries

######## Retrieving time entries

- Endpoint -> https://app.apacta.com/api/v1/time_entries
- Action -> GET
- Description -> When we will send a get request to get the time entries then get request return the time entries and 
  then verify status code as 200
- Response Status Code -> 200
- Response -> {
  "_searchParams": [],
  "_isSearch": false,
  "viewVar": "timeEntries",
  "timeEntries": [
  {
  "id": "2e5fa72d-49e1-4a4d-ae3a-8870fd1c4a88",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "form_id": "bf074ece-e0ea-477b-bbfe-c0f7b432106d",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "time_entry_type_id": null,
  "date": "2022-04-08T00:00:00+00:00",
  "from_time": null,
  "to_time": null,
  "sum": 15840,
  "is_all_day": true,
  "is_calculated": false,
  "created": "2022-04-09T17:39:24+00:00",
  "modified": "2022-04-09T17:39:24+00:00",
  "deleted": null,
  "time_entry_type": null,
  "project": {
  "id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "contact_id": null,
  "contact_person_id": null,
  "parent_id": null,
  "city_id": null,
  "project_status_id": "bea03f03-42ba-4f5c-91e7-4609f86e00a8",
  "pre_calculation_id": null,
  "shared_project_id": null,
  "offer_id": null,
  "project_number": "1",
  "name": "12",
  "description": null,
  "street_name": null,
  "latitude": null,
  "longitude": null,
  "erp_project_id": null,
  "erp_task_id": null,
  "start_time": null,
  "end_time": null,
  "first_activity_date": "2022-04-08T00:00:00+00:00",
  "is_fixed_price": false,
  "working_hours_total_cost_price": 0,
  "products_total_cost_price": 0,
  "total_sales_price": 0,
  "not_invoiced_amount": 0,
  "project_image_url": null,
  "deleted": null,
  "created": "2022-04-08T17:02:06+00:00",
  "modified": "2022-04-10T19:46:02+00:00",
  "full_name": "#1 12",
  "is_offer": false,
  "is_rotten": false,
  "thumbnail": "",
  "has_final_invoice": false
  },
  "user": {
  "id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "city_id": null,
  "country_id": null,
  "language_id": "febce647-b1d3-11e4-b2b9-f23c91842e2b",
  "created_by_id": null,
  "contact_id": null,
  "vendor_id": null,
  "stock_location_id": null,
  "time_entry_rule_group_id": null,
  "email": "deepti.pandit@ritsbv.com",
  "first_name": "Ritsbv",
  "last_name": null,
  "initials": null,
  "image": null,
  "street_name": "",
  "website": null,
  "mobile": "0657538848",
  "mobile_countrycode": "47",
  "cost_price": null,
  "extra_price": null,
  "sale_price": null,
  "is_project_leader": false,
  "is_active": true,
  "is_view_only": false,
  "erp_id": null,
  "api_key": "2577a97e-bb4a-4ce4-8d39-da6d6c810ff9",
  "rfid_key": null,
  "token_expires": "2022-04-09T00:59:58+00:00",
  "receive_form_mails": null,
  "hex_code": null,
  "hide_address": false,
  "hide_phone": false,
  "deleted": null,
  "created": "2022-04-08T16:21:22+00:00",
  "modified": "2022-04-08T17:00:24+00:00",
  "full_name": "Ritsbv",
  "total_cost_price": 0,
  "image_url": null,
  "expected_billable_hours": 0
  }
  },

######## Creating time entries with valid input parameters and return 200

- Endpoint -> https://app.apacta.com/api/v1/time_entries
- Action -> POST
- Description -> When we will send a post request with valid data then we get success with status code 200
- Request Input parameter ->   {
  "form_id": "610fd76b-1ae2-4784-97d0-e3b92e5bfd26",
  "from_time": "",
  "is_all_day": false,
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "sum": 31500,
  "time_entry_type_id": "Integer",
  "to_time": "",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e"
  }
- Response Status Code -> 200
- Response -> {
  "viewVar": "timeEntry",
  "timeEntry": {
  "form_id": "610fd76b-1ae2-4784-97d0-e3b92e5bfd26",
  "from_time": null,
  "is_all_day": false,
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "sum": 31500,
  "time_entry_type_id": "Integer",
  "to_time": null,
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e"
  },
  "success": true
  }

######## Creating time entries with invalid input parameters and return 404

- Endpoint -> https://app.apacta.com/api/v1/time_entries
- Action -> Post
- Description -> When we will send a post request with invalid data then we will get the 404 not found exception
  , and we will verify the status code as 404
- Request Input parameter ->
  {
  "form_id": "610fd76b-1ae2-4784-97d0-e3b92e5bfd26",
  "from_time": "",
  "is_all_day": false,
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "sum": 31500,
  "time_entry_type_id": "Integer",
  "to_time": "",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c23"
  }

- Response Status Code -> 404
- Response -> {"message": "Not Found"}


### Validate retrieving city

- Endpoint -> https://app.apacta.com/api/v1/cities
- Action -> GET
- Description -> When we will send a get request with to retrieve city then we verify status code 200
- Response Status Code -> 200
- Response ->
  {
  "viewVar": "city",
  "city": {
  "id": "1bad7ef2-2c39-4662-b1fe-f21a5ac0b307",
  "country_id": "25d0065f-628d-4583-a47e-862cabc1bd2e",
  "zip_code": "7950",
  "name": "ABELVÆR",
  "deleted": null,
  "pretty_string": "7950 ABELVÆR"
  },
  "success": true
  }

### Validate retrieving and creating contact

######## Retrieving Contacts

- Endpoint -> https://app.apacta.com/api/v1/contacts
- Action -> Get
- Description -> When we will send a get request then it will retrieve contact details and verify status code 200.
- Request Input parameter -> set api key
- Response Status Code -> 202
- Response -> {
  "_searchParams": [],
  "_isSearch": false,
  "viewVar": "contacts",
  "contacts": [
  {
  "id": "e115d3f0-9823-43a9-882e-9262cbdd4005",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "country_id": null,
  "city_id": "bb1c6cd4-74fa-4c01-9e41-102952511f81",
  "payment_term_id": "76e40c2d-d081-4db0-9bba-7371c4965880",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "name": "12 Keys Deepti",
  "description": "",
  "address": "Vilhelm Bergsøes Alle 3, 1. tv.",
  "latitude": "55.73482",
  "longitude": "12.51494",
  "email": "deepti.pandit@ritsbv.com",
  "website": "",
  "phone": "",
  "phone_countrycode": null,
  "cvr": "12",
  "ean": "",
  "erp_id": null,
  "tripletex_id": null,
  "centiga_id": null,
  "pogo_id": null,
  "mg_identifier": null,
  "erp_payment_term_id": null,
  "erp_days_of_credit": null,
  "deleted": null,
  "created": "2022-04-09T03:51:06+00:00",
  "modified": "2022-04-09T03:51:06+00:00",
  "city": {
  "id": "bb1c6cd4-74fa-4c01-9e41-102952511f81",
  "country_id": "25d0065f-628d-4583-a47e-862cabc1bd2e",
  "zip_code": "2860",
  "name": "HOV",
  "deleted": null,
  "pretty_string": "2860 HOV"
  }
  }
  ],
  "success": true
  }

######## Creating Contacts

- Endpoint -> https://app.apacta.com/api/v1/contacts
- Action -> POST
- Description -> When we will send a post request with valid input parameter then it will create the contact and verify the status code 200
- Request Input parameter ->  {
  "address": "amsterdam",
  "city_id": "bb1c6cd4-74fa-4c01-9e41-102952511f81",
  "cvr": "abc",
  "description": "ams contract",
  "email": "deepti@gmail.com",
  "erp_id": "null"
  }
- Response Status Code -> 200
- Response -> {
  "viewVar": "contact",
  "contact": {
  "address": "amsterdam",
  "city_id": "bb1c6cd4-74fa-4c01-9e41-102952511f82",
  "cvr": "abc",
  "description": "ams contract",
  "email": "deepti@gmail.com",
  "erp_id": "null"
  },
  "success": true
  }


### Validate retrieving forms and single form with form id

######## Retrieving forms

- Endpoint -> https://app.apacta.com/api/v1/forms
- Action -> GET
- Description -> When we will send a get request then it will retrieve the forms, verify the status code 200
- Response Status Code -> 200
- Response -> {
  "viewVar": "forms",
  "forms": [
  {
  "id": "442e2795-4d2a-4764-a796-9037195aef05",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "47554724-3b0e-4016-880e-93fa7470d944",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": null,
  "form_date": "2022-04-08T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-08T17:37:22+00:00",
  "created": "2022-04-08T17:36:53+00:00",
  "modified": "2022-04-08T17:37:22+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/442e2795-4d2a-4764-a796-9037195aef05.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/442e2795-4d2a-4764-a796-9037195aef05",
  "is_edited": false
  },
  {
  "id": "610fd76b-1ae2-4784-97d0-e3b92e5bfd26",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "0c5831f0-0ca5-44ae-a263-c36994e931bc",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": null,
  "form_date": "2022-04-08T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-08T17:22:10+00:00",
  "created": "2022-04-08T17:13:06+00:00",
  "modified": "2022-04-08T17:22:10+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/610fd76b-1ae2-4784-97d0-e3b92e5bfd26.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/610fd76b-1ae2-4784-97d0-e3b92e5bfd26",
  "is_edited": false
  },
  {
  "id": "a515894e-4fee-435c-9ece-68cb620ce8c3",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "0e301f74-f6ce-4123-931d-ba51de90055d",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": "be4ff946-c678-4d65-b882-f5d08f8b2dd4",
  "form_date": "2022-04-08T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-08T17:36:02+00:00",
  "created": "2022-04-08T17:02:06+00:00",
  "modified": "2022-04-08T17:36:02+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/a515894e-4fee-435c-9ece-68cb620ce8c3.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/a515894e-4fee-435c-9ece-68cb620ce8c3",
  "is_edited": "2022-04-08T17:36:02+00:00"
  },
  {
  "id": "bf074ece-e0ea-477b-bbfe-c0f7b432106d",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "0e301f74-f6ce-4123-931d-ba51de90055d",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": "a0eaa771-9688-4c97-a80b-958ae7a57e3a",
  "form_date": "2022-04-08T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-09T17:39:24+00:00",
  "created": "2022-04-08T17:36:02+00:00",
  "modified": "2022-04-09T17:39:24+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/bf074ece-e0ea-477b-bbfe-c0f7b432106d.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/bf074ece-e0ea-477b-bbfe-c0f7b432106d",
  "is_edited": "2022-04-09T17:39:24+00:00"
  },
  {
  "id": "c74269e0-7d3b-4a40-bdac-a25a8e8e0802",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "0c5831f0-0ca5-44ae-a263-c36994e931bc",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": null,
  "form_date": "2022-04-08T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-08T17:36:39+00:00",
  "created": "2022-04-08T17:36:14+00:00",
  "modified": "2022-04-08T17:36:39+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/c74269e0-7d3b-4a40-bdac-a25a8e8e0802.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/c74269e0-7d3b-4a40-bdac-a25a8e8e0802",
  "is_edited": false
  },
  {
  "id": "e999be3e-6f66-4707-a181-6c30e72cdefc",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "0e301f74-f6ce-4123-931d-ba51de90055d",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": "a0eaa771-9688-4c97-a80b-958ae7a57e3a",
  "form_date": "2022-04-09T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-09T17:39:24+00:00",
  "created": "2022-04-09T17:39:24+00:00",
  "modified": "2022-04-09T17:39:24+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/e999be3e-6f66-4707-a181-6c30e72cdefc.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/e999be3e-6f66-4707-a181-6c30e72cdefc",
  "is_edited": false
  }
  ],
  "success": true
  }

######## Retrieving form with form_id

- Endpoint ->  https://app.apacta.com/api/v1/forms/{form_id}
- Action -> GET
- Description -> When we will send a get request with valid form_id then we will retrieve the form with status code 200 .
- Request Input parameter -> form_id=442e2795-4d2a-4764-a796-9037195aef05
- Response Status Code -> 404
- Response -> {
  "viewVar": "form",
  "form": {
  "id": "442e2795-4d2a-4764-a796-9037195aef05",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "modified_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "user_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "project_id": "0025a01c-c176-4308-87d7-e19f457ba003",
  "form_template_id": "47554724-3b0e-4016-880e-93fa7470d944",
  "mass_form_id": null,
  "approved_by_id": null,
  "activity_id": null,
  "clocking_record_id": null,
  "form_date": "2022-04-08T00:00:00+00:00",
  "is_invoiced": null,
  "is_draft": false,
  "is_shared": false,
  "shared_with_contact": false,
  "shared_with_vendor": false,
  "approved": "2022-04-08T17:37:22+00:00",
  "created": "2022-04-08T17:36:53+00:00",
  "modified": "2022-04-08T17:37:22+00:00",
  "deleted": null,
  "link_to_pdf": "https://app.apacta.com/companies/forms/view/442e2795-4d2a-4764-a796-9037195aef05.pdf",
  "link_to_html": "https://app.apacta.com/companies/forms/view/442e2795-4d2a-4764-a796-9037195aef05",
  "is_edited": false
  },
  "success": true
  }

### Validate retrieving and deleting invoices with authorized user

######## Retrieving invoices with authorized user

- Endpoint -> https://app.apacta.com/api/v1/invoices
- Action -> GET
- Description -> When we will send a get request to get the invoices then get request return the invoices and
  then verify status code as 200
- Response Status Code -> 200
- Response -> {
  "_searchParams": [],
  "_isSearch": false,
  "viewVar": "invoices",
  "invoices": [
  {
  "id": "9148a250-732b-49b3-a94f-699b05fd9e9f",
  "contact_id": null,
  "sender_id": null,
  "project_id": "56045e02-b434-4c88-8386-cc19689fbd62",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "payment_term_id": "76e40c2d-d081-4db0-9bba-7371c4965880",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "integration_id": null,
  "invoice_number": null,
  "offer_number": null,
  "title": null,
  "message": "#2 #1 12\nFrom Saturday, April 9, 2022 to Saturday, April 9, 2022\n",
  "reference": null,
  "issued_date": "2022-04-09T00:00:00+00:00",
  "payment_due_date": null,
  "date_from": "2022-04-09T00:00:00+00:00",
  "date_to": "2022-04-09T00:00:00+00:00",
  "is_draft": true,
  "is_offer": false,
  "is_locked": false,
  "is_final_invoice": false,
  "vat_percent": 25,
  "discount_percent": 0,
  "group_by_forms": false,
  "erp_id": null,
  "erp_payment_term_id": null,
  "project_overview_attached": false,
  "eu_customer": true,
  "downloaded": null,
  "include_invoiced_forms": false,
  "combine_product_lines": false,
  "combine_working_time_lines": false,
  "show_payment_term": true,
  "show_prices": true,
  "show_product_images": false,
  "show_products_product_bundle": true,
  "show_employee_name": false,
  "deleted": null,
  "created": "2022-04-09T03:50:40+00:00",
  "modified": "2022-04-09T03:50:40+00:00",
  "pdf_url": null,
  "gross_payment": 0,
  "net_payment": 0,
  "total_discount_percent": 0,
  "has_project_pdf_attached": false,
  "total_cost_price": 0
  }
  ],
  "success": true
  }

######## Deleting invoices with authorized user

- Endpoint -> https://app.apacta.com/api/v1/invoices
- Action -> DELETE
- Description -> When we will send a delete request to delete the invoices then it will delete the invoices and
  then verify status code as 200
- Response Status Code -> 200

######## Retrieving invoices with unauthorized user

- Endpoint -> https://app.apacta.com/api/v1/invoices
- Action -> GET
- Description -> When we will send a get request with invalid apikey then we will get the invalid API Key error message
- Request Input parameter -> apikey=invalid
- Response Status Code -> 401
- Response -> {"status": 401,"message": "Unauthorized"}

### Validate retrieving product

- Endpoint -> https://app.apacta.com/api/v1/products
- Action -> GET
- Description -> When we will send a get request with to retrieve products then we verify status code 200
- Response Status Code -> 200
- Response ->
  {
  "_searchParams": [],
  "_isSearch": false,
  "viewVar": "products",
  "products": [
  {
  "id": "98f2fcf9-897e-439f-92d8-47dee6751000",
  "company_id": "aadd24d6-cffd-4bef-b053-3d35764418aa",
  "created_by_id": "29391f53-c139-4b3a-a1a0-c86d39e29c2e",
  "origin": "manually_created",
  "price_type": "manual",
  "name": "emp management system",
  "nickname": null,
  "description": "",
  "product_number": "",
  "barcode": "",
  "product_rule": null,
  "buying_price": 100000,
  "selling_price": 100000,
  "is_manual_buying_price": true,
  "is_manual_selling_price": true,
  "image": null,
  "erp_id": null,
  "tripletex_id": null,
  "centiga_id": null,
  "pogo_id": null,
  "version_id": 1,
  "deleted": null,
  "created": "2022-04-09T03:52:01+00:00",
  "modified": "2022-04-09T03:52:01+00:00",
  "image_url": null,
  "translated_origin": "Manually created"
  }
  ],
  "success": true
  }