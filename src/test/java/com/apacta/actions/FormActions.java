package com.apacta.actions;

import com.apacta.models.Form;
import com.apacta.models.Forms;
import com.apacta.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.lang.reflect.Type;

public class FormActions {
    public Response response;
    private static final String API_ENDPOINT = "?api_key=";
    private static final String FORM_MANAGEMENT_ENDPOINT = "forms";

    @Step("Retrieve form")
    public void requestFormWithGetMethod() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, FORM_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve response body for Get form")
    public Forms getBody() throws Exception {
        Forms forms = response.then().extract().as((Type) Forms.class);
        return forms;
    }

    public void requestFormByProvidingFormIdWithGetMethod(String form_id) throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s/%s%s%s", Constants.BASE_URL, FORM_MANAGEMENT_ENDPOINT, form_id, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve response body for Get form with Form id")
    public Form getFormBody() throws Exception {
        Form form = response.then().extract().as((Type) Form.class);
        return form;
    }
}
