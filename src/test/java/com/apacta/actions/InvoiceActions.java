package com.apacta.actions;

import com.apacta.models.Invoice;
import com.apacta.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.lang.reflect.Type;

public class InvoiceActions {
    public Response response;
    private static final String API_ENDPOINT = "?api_key=";
    private static final String INVOICE_MANAGEMENT_ENDPOINT = "invoices";

    @Step("Retrieve invoice")
    public void requestInvoiceWithGetMethod() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, INVOICE_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve response body for Get invoices")
    public Invoice getBody() throws Exception {
        Invoice invoice = response.then().extract().as((Type) Invoice.class);
        return invoice;
    }

    @Step("Delete invoice")
    public void requestInvoiceWithDeleteMethod() throws Exception {
        response = SerenityRest.given()
                .delete(String.format("%s/%s%s%s", Constants.BASE_URL, INVOICE_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve invoice with unauthorized user")
    public void requestInvoiceWithGetMethodForUnauthorizedUser() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, INVOICE_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY_UNAUTHORIZED));
    }
}
