package com.apacta.actions;

import com.apacta.models.Product;
import com.apacta.utils.Constants;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.lang.reflect.Type;

public class ProductActions {
    public Response response;
    private static final String API_ENDPOINT = "?api_key=";
    private static final String PRODUCT_MANAGEMENT_ENDPOINT = "products";

    @Step("Retrieve product")
    public void requestProductWithGetMethod() throws Exception {
        response = SerenityRest.given()
                .get(String.format("%s/%s%s%s", Constants.BASE_URL, PRODUCT_MANAGEMENT_ENDPOINT, API_ENDPOINT, Constants.API_KEY));
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve response body for Get product")
    public Product getBody() throws Exception {
        Product product = response.then().extract().as((Type) Product.class);
        return product;
    }
}
