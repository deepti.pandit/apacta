/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;
import java.util.UUID;

/**
 * Contact
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class ContactDto {

    public static final String SERIALIZED_NAME_ADDRESS = "address";
    public static final String SERIALIZED_NAME_CITY_ID = "city_id";
    public static final String SERIALIZED_NAME_COMPANY_ID = "company_id";
    public static final String SERIALIZED_NAME_CREATED = "created";
    public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
    public static final String SERIALIZED_NAME_CVR = "cvr";
    public static final String SERIALIZED_NAME_DELETED = "deleted";
    public static final String SERIALIZED_NAME_DESCRIPTION = "description";
    public static final String SERIALIZED_NAME_EMAIL = "email";
    public static final String SERIALIZED_NAME_ERP_ID = "erp_id";
    public static final String SERIALIZED_NAME_ID = "id";
    public static final String SERIALIZED_NAME_MODIFIED = "modified";
    public static final String SERIALIZED_NAME_NAME = "name";
    public static final String SERIALIZED_NAME_PHONE = "phone";
    public static final String SERIALIZED_NAME_WEBSITE = "website";
    /**
     * READ-ONLY
     *
     * @return city
     **/

    public static final String SERIALIZED_NAME_CITY = "city";
    public String created_by_id;
    public String country_id;
    public String city_id;
    public String payment_term_id;
    public String company_id;
    public String latitude;
    public String longitude;
    public String phone_countrycode;
    public String ean;
    public String tripletex_id;
    public String centiga_id;
    public String pogo_id;
    public String mg_identifier;
    public String erp_payment_term_id;
    public String erp_days_of_credit;
    @SerializedName(SERIALIZED_NAME_ADDRESS)
    private String address;
    @SerializedName(SERIALIZED_NAME_CITY_ID)
    private UUID cityId;
    @SerializedName(SERIALIZED_NAME_COMPANY_ID)
    private UUID companyId;
    @SerializedName(SERIALIZED_NAME_CREATED)
    private String created;
    @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
    private UUID createdById;
    @SerializedName(SERIALIZED_NAME_CVR)
    private String cvr;
    @SerializedName(SERIALIZED_NAME_DELETED)
    private String deleted;
    @SerializedName(SERIALIZED_NAME_DESCRIPTION)
    private String description;
    @SerializedName(SERIALIZED_NAME_EMAIL)
    private String email;
    @SerializedName(SERIALIZED_NAME_ERP_ID)
    private String erp_id;
    @SerializedName(SERIALIZED_NAME_ID)
    private UUID id;
    @SerializedName(SERIALIZED_NAME_MODIFIED)
    private String modified;
    @SerializedName(SERIALIZED_NAME_NAME)
    private String name;
    @SerializedName(SERIALIZED_NAME_PHONE)
    private String phone;
    @SerializedName(SERIALIZED_NAME_WEBSITE)
    private String website;
    @SerializedName(SERIALIZED_NAME_CITY)
    private CityDto city = null;

    public ContactDto address(String address) {

        this.address = address;
        return this;
    }

    /**
     * Street address
     *
     * @return address
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Street address")

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ContactDto cityId(UUID cityId) {

        this.cityId = cityId;
        return this;
    }

    public ContactDto city(CityDto city) {
        this.city = city;
        return this;
    }

    public ContactDto addCity(CityDto city) {
        this.city = city;
        return this;
    }

    @javax.annotation.Nullable
    @ApiModelProperty(value = "")
    public CityDto getCity() {
        return city;
    }

    public void setCity(CityDto city) {
        this.city = city;
    }

    /**
     * Get cityId
     *
     * @return cityId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getCityId() {
        return cityId;
    }


    public void setCityId(UUID cityId) {
        this.cityId = cityId;
    }


    public ContactDto companyId(UUID companyId) {

        this.companyId = companyId;
        return this;
    }

    /**
     * Only filled out if this represents another company within Apacta (used for partners)
     *
     * @return companyId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Only filled out if this represents another company within Apacta (used for partners)")

    public UUID getCompanyId() {
        return companyId;
    }


    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }


    public ContactDto created(String created) {

        this.created = created;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return created
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public ContactDto createdById(UUID createdById) {

        this.createdById = createdById;
        return this;
    }

    /**
     * Read-only
     *
     * @return createdById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Read-only")

    public UUID getCreatedById() {
        return createdById;
    }


    public void setCreatedById(UUID createdById) {
        this.createdById = createdById;
    }


    public ContactDto cvr(String cvr) {

        this.cvr = cvr;
        return this;
    }

    /**
     * Get cvr
     *
     * @return cvr
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getCvr() {
        return cvr;
    }


    public void setCvr(String cvr) {
        this.cvr = cvr;
    }


    public ContactDto deleted(String deleted) {

        this.deleted = deleted;
        return this;
    }

    /**
     * READ-ONLY - only present if it&#39;s an deleted object
     *
     * @return deleted
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

    public String getDeleted() {
        return deleted;
    }


    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }


    public ContactDto description(String description) {

        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getDescription() {
        return description;
    }


    public void setDescription(String description) {
        this.description = description;
    }


    public ContactDto email(String email) {

        this.email = email;
        return this;
    }

    /**
     * Get email
     *
     * @return email
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public ContactDto erp_id(String erp_id) {

        this.erp_id = erp_id;
        return this;
    }

    /**
     * If company has integration to an ERP system, and the contacts are synchronized, this will be the ERP-systems ID of this contact
     *
     * @return erp_id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "If company has integration to an ERP system, and the contacts are synchronized, this will be the ERP-systems ID of this contact")

    public String getErp_id() {
        return erp_id;
    }


    public void setErp_id(String erp_id) {
        this.erp_id = erp_id;
    }


    public ContactDto id(UUID id) {

        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getId() {
        return id;
    }


    public void setId(UUID id) {
        this.id = id;
    }


    public ContactDto modified(String modified) {

        this.modified = modified;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return modified
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getModified() {
        return modified;
    }


    public void setModified(String modified) {
        this.modified = modified;
    }


    public ContactDto name(String name) {

        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public ContactDto phone(String phone) {

        this.phone = phone;
        return this;
    }

    /**
     * Format like eg. &#x60;28680133&#x60; or &#x60;046158971404&#x60;
     *
     * @return phone
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Format like eg. `28680133` or `046158971404`")

    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }


    public ContactDto website(String website) {

        this.website = website;
        return this;
    }

    /**
     * Get website
     *
     * @return website
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getWebsite() {
        return website;
    }


    public void setWebsite(String website) {
        this.website = website;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ContactDto contact = (ContactDto) o;
        return Objects.equals(this.address, contact.address) &&
                Objects.equals(this.cityId, contact.cityId) &&
                Objects.equals(this.companyId, contact.companyId) &&
                Objects.equals(this.created, contact.created) &&
                Objects.equals(this.createdById, contact.createdById) &&
                Objects.equals(this.cvr, contact.cvr) &&
                Objects.equals(this.deleted, contact.deleted) &&
                Objects.equals(this.description, contact.description) &&
                Objects.equals(this.email, contact.email) &&
                Objects.equals(this.erp_id, contact.erp_id) &&
                Objects.equals(this.id, contact.id) &&
                Objects.equals(this.modified, contact.modified) &&
                Objects.equals(this.name, contact.name) &&
                Objects.equals(this.phone, contact.phone) &&
                Objects.equals(this.website, contact.website);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, cityId, companyId, created, createdById, cvr, deleted, description, email, erp_id, id, modified, name, phone, website);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Contact {\n");
        sb.append("    address: ").append(toIndentedString(address)).append("\n");
        sb.append("    cityId: ").append(toIndentedString(cityId)).append("\n");
        sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
        sb.append("    created: ").append(toIndentedString(created)).append("\n");
        sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
        sb.append("    cvr: ").append(toIndentedString(cvr)).append("\n");
        sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    email: ").append(toIndentedString(email)).append("\n");
        sb.append("    erp_id: ").append(toIndentedString(erp_id)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
        sb.append("    website: ").append(toIndentedString(website)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    public String toJSONString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" {\n");
        sb.append(" \"address\": ").append("\"" + toIndentedString(address) + "\"").append(",\n");
        sb.append(" \"city_id\": ").append("\"" + toIndentedString(cityId) + "\"").append(",\n");
        sb.append(" \"cvr\": ").append("\"" + toIndentedString(cvr) + "\"").append(",\n");
        sb.append(" \"description\": ").append("\"" + toIndentedString(description) + "\"").append(",\n");
        sb.append(" \"email\": ").append("\"" + toIndentedString(email) + "\"").append(",\n");
        sb.append(" \"erp_id\": ").append("\"" + toIndentedString(erp_id) + "\"").append("\n");
        sb.append("}");

        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}

