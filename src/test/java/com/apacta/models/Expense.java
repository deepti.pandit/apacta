/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;
import org.threeten.bp.LocalDate;

/**
 * Expense
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class Expense {
  public static final String SERIALIZED_NAME_COMPANY_ID = "company_id";
  @SerializedName(SERIALIZED_NAME_COMPANY_ID)
  private UUID companyId;

  public static final String SERIALIZED_NAME_CONTACT_ID = "contact_id";
  @SerializedName(SERIALIZED_NAME_CONTACT_ID)
  private UUID contactId;

  public static final String SERIALIZED_NAME_CREATED = "created";
  @SerializedName(SERIALIZED_NAME_CREATED)
  private String created;

  public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
  @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
  private UUID createdById;

  public static final String SERIALIZED_NAME_CURRENCY_ID = "currency_id";
  @SerializedName(SERIALIZED_NAME_CURRENCY_ID)
  private UUID currencyId;

  public static final String SERIALIZED_NAME_DELETED = "deleted";
  @SerializedName(SERIALIZED_NAME_DELETED)
  private String deleted;

  public static final String SERIALIZED_NAME_DELIVERY_DATE = "delivery_date";
  @SerializedName(SERIALIZED_NAME_DELIVERY_DATE)
  private LocalDate deliveryDate;

  public static final String SERIALIZED_NAME_DESCRIPTION = "description";
  @SerializedName(SERIALIZED_NAME_DESCRIPTION)
  private String description;

  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private UUID id;

  public static final String SERIALIZED_NAME_MODIFIED = "modified";
  @SerializedName(SERIALIZED_NAME_MODIFIED)
  private String modified;

  public static final String SERIALIZED_NAME_PROJECT_ID = "project_id";
  @SerializedName(SERIALIZED_NAME_PROJECT_ID)
  private UUID projectId;

  public static final String SERIALIZED_NAME_REFERENCE = "reference";
  @SerializedName(SERIALIZED_NAME_REFERENCE)
  private String reference;

  public static final String SERIALIZED_NAME_SHORT_TEXT = "short_text";
  @SerializedName(SERIALIZED_NAME_SHORT_TEXT)
  private String shortText;

  public static final String SERIALIZED_NAME_SUPPLIER_INVOICE_NUMBER = "supplier_invoice_number";
  @SerializedName(SERIALIZED_NAME_SUPPLIER_INVOICE_NUMBER)
  private String supplierInvoiceNumber;


  public Expense companyId(UUID companyId) {
    
    this.companyId = companyId;
    return this;
  }

   /**
   * Read-only
   * @return companyId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Read-only")

  public UUID getCompanyId() {
    return companyId;
  }



  public void setCompanyId(UUID companyId) {
    this.companyId = companyId;
  }


  public Expense contactId(UUID contactId) {
    
    this.contactId = contactId;
    return this;
  }

   /**
   * Get contactId
   * @return contactId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getContactId() {
    return contactId;
  }



  public void setContactId(UUID contactId) {
    this.contactId = contactId;
  }


  public Expense created(String created) {
    
    this.created = created;
    return this;
  }

   /**
   * READ-ONLY
   * @return created
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getCreated() {
    return created;
  }



  public void setCreated(String created) {
    this.created = created;
  }


  public Expense createdById(UUID createdById) {
    
    this.createdById = createdById;
    return this;
  }

   /**
   * Read-only
   * @return createdById
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Read-only")

  public UUID getCreatedById() {
    return createdById;
  }



  public void setCreatedById(UUID createdById) {
    this.createdById = createdById;
  }


  public Expense currencyId(UUID currencyId) {
    
    this.currencyId = currencyId;
    return this;
  }

   /**
   * Get currencyId
   * @return currencyId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getCurrencyId() {
    return currencyId;
  }



  public void setCurrencyId(UUID currencyId) {
    this.currencyId = currencyId;
  }


  public Expense deleted(String deleted) {
    
    this.deleted = deleted;
    return this;
  }

   /**
   * READ-ONLY - only present if it&#39;s an deleted object
   * @return deleted
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

  public String getDeleted() {
    return deleted;
  }



  public void setDeleted(String deleted) {
    this.deleted = deleted;
  }


  public Expense deliveryDate(LocalDate deliveryDate) {
    
    this.deliveryDate = deliveryDate;
    return this;
  }

   /**
   * Get deliveryDate
   * @return deliveryDate
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public LocalDate getDeliveryDate() {
    return deliveryDate;
  }



  public void setDeliveryDate(LocalDate deliveryDate) {
    this.deliveryDate = deliveryDate;
  }


  public Expense description(String description) {
    
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getDescription() {
    return description;
  }



  public void setDescription(String description) {
    this.description = description;
  }


  public Expense id(UUID id) {
    
    this.id = id;
    return this;
  }

   /**
   * Read-only
   * @return id
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Read-only")

  public UUID getId() {
    return id;
  }



  public void setId(UUID id) {
    this.id = id;
  }


  public Expense modified(String modified) {
    
    this.modified = modified;
    return this;
  }

   /**
   * READ-ONLY
   * @return modified
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getModified() {
    return modified;
  }



  public void setModified(String modified) {
    this.modified = modified;
  }


  public Expense projectId(UUID projectId) {
    
    this.projectId = projectId;
    return this;
  }

   /**
   * Get projectId
   * @return projectId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getProjectId() {
    return projectId;
  }



  public void setProjectId(UUID projectId) {
    this.projectId = projectId;
  }


  public Expense reference(String reference) {
    
    this.reference = reference;
    return this;
  }

   /**
   * Get reference
   * @return reference
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getReference() {
    return reference;
  }



  public void setReference(String reference) {
    this.reference = reference;
  }


  public Expense shortText(String shortText) {
    
    this.shortText = shortText;
    return this;
  }

   /**
   * Get shortText
   * @return shortText
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getShortText() {
    return shortText;
  }



  public void setShortText(String shortText) {
    this.shortText = shortText;
  }


  public Expense supplierInvoiceNumber(String supplierInvoiceNumber) {
    
    this.supplierInvoiceNumber = supplierInvoiceNumber;
    return this;
  }

   /**
   * Get supplierInvoiceNumber
   * @return supplierInvoiceNumber
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getSupplierInvoiceNumber() {
    return supplierInvoiceNumber;
  }



  public void setSupplierInvoiceNumber(String supplierInvoiceNumber) {
    this.supplierInvoiceNumber = supplierInvoiceNumber;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Expense expense = (Expense) o;
    return Objects.equals(this.companyId, expense.companyId) &&
        Objects.equals(this.contactId, expense.contactId) &&
        Objects.equals(this.created, expense.created) &&
        Objects.equals(this.createdById, expense.createdById) &&
        Objects.equals(this.currencyId, expense.currencyId) &&
        Objects.equals(this.deleted, expense.deleted) &&
        Objects.equals(this.deliveryDate, expense.deliveryDate) &&
        Objects.equals(this.description, expense.description) &&
        Objects.equals(this.id, expense.id) &&
        Objects.equals(this.modified, expense.modified) &&
        Objects.equals(this.projectId, expense.projectId) &&
        Objects.equals(this.reference, expense.reference) &&
        Objects.equals(this.shortText, expense.shortText) &&
        Objects.equals(this.supplierInvoiceNumber, expense.supplierInvoiceNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(companyId, contactId, created, createdById, currencyId, deleted, deliveryDate, description, id, modified, projectId, reference, shortText, supplierInvoiceNumber);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Expense {\n");
    sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
    sb.append("    contactId: ").append(toIndentedString(contactId)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
    sb.append("    currencyId: ").append(toIndentedString(currencyId)).append("\n");
    sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
    sb.append("    deliveryDate: ").append(toIndentedString(deliveryDate)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
    sb.append("    projectId: ").append(toIndentedString(projectId)).append("\n");
    sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
    sb.append("    shortText: ").append(toIndentedString(shortText)).append("\n");
    sb.append("    supplierInvoiceNumber: ").append(toIndentedString(supplierInvoiceNumber)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

