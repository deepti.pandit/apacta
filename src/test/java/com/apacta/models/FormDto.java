/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.LocalDate;

import java.util.Objects;
import java.util.UUID;

/**
 * Form
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class FormDto {

    public static final String SERIALIZED_NAME_APPROVED_BY_ID = "approved_by_id";
    public static final String SERIALIZED_NAME_COMPANY_ID = "company_id";
    public static final String SERIALIZED_NAME_CREATED = "created";
    public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
    public static final String SERIALIZED_NAME_DELETED = "deleted";
    public static final String SERIALIZED_NAME_FORM_DATE = "form_date";
    public static final String SERIALIZED_NAME_FORM_TEMPLATE_ID = "form_template_id";
    public static final String SERIALIZED_NAME_ID = "id";
    public static final String SERIALIZED_NAME_IS_DRAFT = "is_draft";
    public static final String SERIALIZED_NAME_IS_INVOICED = "is_invoiced";
    public static final String SERIALIZED_NAME_IS_SHARED = "is_shared";
    public static final String SERIALIZED_NAME_MASS_FORM_ID = "mass_form_id";
    public static final String SERIALIZED_NAME_MODIFIED = "modified";
    public static final String SERIALIZED_NAME_PROJECT_ID = "project_id";
    public String created_by_id;
    public String modified_by_id;
    public String user_id;
    public String project_id;
    public String form_template_id;
    public String mass_form_id;
    public String approved_by_id;
    public String activity_id;
    public String clocking_record_id;
    public String form_date;
    public String is_invoiced;
    public String is_draft;
    public String is_shared;
    public String shared_with_contact;
    public String shared_with_vendor;
    public String approved;
    public String link_to_pdf;
    public String link_to_html;
    public String is_edited;
    @SerializedName(SERIALIZED_NAME_APPROVED_BY_ID)
    private UUID approvedById;
    @SerializedName(SERIALIZED_NAME_COMPANY_ID)
    private String company_id;
    @SerializedName(SERIALIZED_NAME_CREATED)
    private String created;
    @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
    private UUID createdById;
    @SerializedName(SERIALIZED_NAME_DELETED)
    private String deleted;
    @SerializedName(SERIALIZED_NAME_FORM_DATE)
    private LocalDate formDate;
    @SerializedName(SERIALIZED_NAME_FORM_TEMPLATE_ID)
    private UUID formTemplateId;
    @SerializedName(SERIALIZED_NAME_ID)
    private UUID id;
    @SerializedName(SERIALIZED_NAME_IS_DRAFT)
    private Boolean isDraft = false;
    @SerializedName(SERIALIZED_NAME_IS_INVOICED)
    private Boolean isInvoiced = false;
    @SerializedName(SERIALIZED_NAME_IS_SHARED)
    private Boolean isShared = false;
    @SerializedName(SERIALIZED_NAME_MASS_FORM_ID)
    private UUID massFormId;
    @SerializedName(SERIALIZED_NAME_MODIFIED)
    private String modified;
    @SerializedName(SERIALIZED_NAME_PROJECT_ID)
    private UUID projectId;


    public FormDto approvedById(UUID approvedById) {

        this.approvedById = approvedById;
        return this;
    }

    /**
     * Get approvedById
     *
     * @return approvedById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getApprovedById() {
        return approvedById;
    }


    public void setApprovedById(UUID approvedById) {
        this.approvedById = approvedById;
    }


    public FormDto company_id(String company_id) {

        this.company_id = company_id;
        return this;
    }

    /**
     * Get company_id
     *
     * @return company_id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getCompany_id() {
        return company_id;
    }


    public void setCompany_idd(String company_id) {
        this.company_id = company_id;
    }


    public FormDto created(String created) {

        this.created = created;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return created
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public FormDto createdById(UUID createdById) {

        this.createdById = createdById;
        return this;
    }

    /**
     * Read-only
     *
     * @return createdById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Read-only")

    public UUID getCreatedById() {
        return createdById;
    }


    public void setCreatedById(UUID createdById) {
        this.createdById = createdById;
    }


    public FormDto deleted(String deleted) {

        this.deleted = deleted;
        return this;
    }

    /**
     * READ-ONLY - only present if it&#39;s an deleted object
     *
     * @return deleted
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

    public String getDeleted() {
        return deleted;
    }


    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }


    public FormDto formDate(LocalDate formDate) {

        this.formDate = formDate;
        return this;
    }

    /**
     * Get formDate
     *
     * @return formDate
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public LocalDate getFormDate() {
        return formDate;
    }


    public void setFormDate(LocalDate formDate) {
        this.formDate = formDate;
    }


    public FormDto formTemplateId(UUID formTemplateId) {

        this.formTemplateId = formTemplateId;
        return this;
    }

    /**
     * Get formTemplateId
     *
     * @return formTemplateId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getFormTemplateId() {
        return formTemplateId;
    }


    public void setFormTemplateId(UUID formTemplateId) {
        this.formTemplateId = formTemplateId;
    }


    public FormDto id(UUID id) {

        this.id = id;
        return this;
    }

    /**
     * Read-only
     *
     * @return id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "Read-only")

    public UUID getId() {
        return id;
    }


    public void setId(UUID id) {
        this.id = id;
    }


    public FormDto isDraft(Boolean isDraft) {

        this.isDraft = isDraft;
        return this;
    }

    /**
     * Get isDraft
     *
     * @return isDraft
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIsDraft() {
        return isDraft;
    }


    public void setIsDraft(Boolean isDraft) {
        this.isDraft = isDraft;
    }


    public FormDto isInvoiced(Boolean isInvoiced) {

        this.isInvoiced = isInvoiced;
        return this;
    }

    /**
     * Get isInvoiced
     *
     * @return isInvoiced
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIsInvoiced() {
        return isInvoiced;
    }


    public void setIsInvoiced(Boolean isInvoiced) {
        this.isInvoiced = isInvoiced;
    }


    public FormDto isShared(Boolean isShared) {

        this.isShared = isShared;
        return this;
    }

    /**
     * Get isShared
     *
     * @return isShared
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIsShared() {
        return isShared;
    }


    public void setIsShared(Boolean isShared) {
        this.isShared = isShared;
    }


    public FormDto massFormId(UUID massFormId) {

        this.massFormId = massFormId;
        return this;
    }

    /**
     * Get massFormId
     *
     * @return massFormId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getMassFormId() {
        return massFormId;
    }


    public void setMassFormId(UUID massFormId) {
        this.massFormId = massFormId;
    }


    public FormDto modified(String modified) {

        this.modified = modified;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return modified
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getModified() {
        return modified;
    }


    public void setModified(String modified) {
        this.modified = modified;
    }


    public FormDto projectId(UUID projectId) {

        this.projectId = projectId;
        return this;
    }

    /**
     * Get projectId
     *
     * @return projectId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getProjectId() {
        return projectId;
    }


    public void setProjectId(UUID projectId) {
        this.projectId = projectId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FormDto form = (FormDto) o;
        return Objects.equals(this.approvedById, form.approvedById) &&
                Objects.equals(this.company_id, form.company_id) &&
                Objects.equals(this.created, form.created) &&
                Objects.equals(this.createdById, form.createdById) &&
                Objects.equals(this.deleted, form.deleted) &&
                Objects.equals(this.formDate, form.formDate) &&
                Objects.equals(this.formTemplateId, form.formTemplateId) &&
                Objects.equals(this.id, form.id) &&
                Objects.equals(this.isDraft, form.isDraft) &&
                Objects.equals(this.isInvoiced, form.isInvoiced) &&
                Objects.equals(this.isShared, form.isShared) &&
                Objects.equals(this.massFormId, form.massFormId) &&
                Objects.equals(this.modified, form.modified) &&
                Objects.equals(this.projectId, form.projectId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(approvedById, company_id, created, createdById, deleted, formDate, formTemplateId, id, isDraft, isInvoiced, isShared, massFormId, modified, projectId);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Form {\n");
        sb.append("    approvedById: ").append(toIndentedString(approvedById)).append("\n");
        sb.append("    company_id: ").append(toIndentedString(company_id)).append("\n");
        sb.append("    created: ").append(toIndentedString(created)).append("\n");
        sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
        sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
        sb.append("    formDate: ").append(toIndentedString(formDate)).append("\n");
        sb.append("    formTemplateId: ").append(toIndentedString(formTemplateId)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    isDraft: ").append(toIndentedString(isDraft)).append("\n");
        sb.append("    isInvoiced: ").append(toIndentedString(isInvoiced)).append("\n");
        sb.append("    isShared: ").append(toIndentedString(isShared)).append("\n");
        sb.append("    massFormId: ").append(toIndentedString(massFormId)).append("\n");
        sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
        sb.append("    projectId: ").append(toIndentedString(projectId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}

