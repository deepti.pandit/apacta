/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.LocalDate;

import java.util.Objects;
import java.util.UUID;

/**
 * Invoice
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class InvoiceDto {
    public static final String SERIALIZED_NAME_COMPANY_ID = "company_id";
    public static final String SERIALIZED_NAME_CONTACT_ID = "contact_id";
    public static final String SERIALIZED_NAME_CREATED = "created";
    public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
    public static final String SERIALIZED_NAME_CURRENCY_ID = "currency_id";
    public static final String SERIALIZED_NAME_DATE_FROM = "date_from";
    public static final String SERIALIZED_NAME_DATE_TO = "date_to";
    public static final String SERIALIZED_NAME_DELETED = "deleted";
    public static final String SERIALIZED_NAME_ERP_ID = "erp_id";
    public static final String SERIALIZED_NAME_ERP_PAYMENT_TERM_ID = "erp_payment_term_id";
    public static final String SERIALIZED_NAME_EU_CUSTOMER = "eu_customer";
    public static final String SERIALIZED_NAME_GROSS_PAYMENT = "gross_payment";
    public static final String SERIALIZED_NAME_ID = "id";
    public static final String SERIALIZED_NAME_INTEGRATION_ID = "integration_id";
    public static final String SERIALIZED_NAME_INVOICE_NUMBER = "invoice_number";
    public static final String SERIALIZED_NAME_IS_DRAFT = "is_draft";
    public static final String SERIALIZED_NAME_IS_LOCKED = "is_locked";
    public static final String SERIALIZED_NAME_IS_OFFER = "is_offer";
    public static final String SERIALIZED_NAME_ISSUED_DATE = "issued_date";
    public static final String SERIALIZED_NAME_MESSAGE = "message";
    public static final String SERIALIZED_NAME_MODIFIED = "modified";
    public static final String SERIALIZED_NAME_NET_PAYMENT = "net_payment";
    public static final String SERIALIZED_NAME_OFFER_NUMBER = "offer_number";
    public static final String SERIALIZED_NAME_PAYMENT_DUE_DATE = "payment_due_date";
    public static final String SERIALIZED_NAME_PAYMENT_TERM_ID = "payment_term_id";
    public static final String SERIALIZED_NAME_PROJECT_ID = "project_id";
    public static final String SERIALIZED_NAME_REFERENCE = "reference";
    public static final String SERIALIZED_NAME_VAT_PERCENT = "vat_percent";
    public String contact_id;
    public String sender_id;
    public String created_by_id;
    public String payment_term_id;
    public String company_id;
    public String integration_id;
    public String invoice_number;
    public String offer_number;
    public String title;
    public String issued_date;
    public String payment_due_date;
    public String date_from;
    public String date_to;
    public String is_draft;
    public String is_offer;
    public String is_locked;
    public String is_final_invoice;
    public String vat_percent;
    public String discount_percent;
    public String group_by_forms;
    public String erp_id;
    public String erp_payment_term_id;
    public String project_overview_attached;
    public String eu_customer;
    public String downloaded;
    public String include_invoiced_forms;
    public String combine_product_lines;
    public String combine_working_time_lines;
    public String show_payment_term;
    public String show_prices;
    public String show_product_images;
    public String show_employee_name;
    public String show_products_product_bundle;
    public String pdf_url;
    public String gross_payment;
    public String net_payment;
    public String total_discount_percent;
    public String has_project_pdf_attached;
    public String total_cost_price;
    @SerializedName(SERIALIZED_NAME_COMPANY_ID)
    private UUID companyId;
    @SerializedName(SERIALIZED_NAME_CONTACT_ID)
    private UUID contactId;
    @SerializedName(SERIALIZED_NAME_CREATED)
    private String created;
    @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
    private UUID createdById;
    @SerializedName(SERIALIZED_NAME_CURRENCY_ID)
    private UUID currencyId;
    @SerializedName(SERIALIZED_NAME_DATE_FROM)
    private LocalDate dateFrom;
    @SerializedName(SERIALIZED_NAME_DATE_TO)
    private LocalDate dateTo;
    @SerializedName(SERIALIZED_NAME_DELETED)
    private String deleted;
    @SerializedName(SERIALIZED_NAME_ERP_ID)
    private String erpId;
    @SerializedName(SERIALIZED_NAME_ERP_PAYMENT_TERM_ID)
    private String erpPaymentTermId;
    @SerializedName(SERIALIZED_NAME_EU_CUSTOMER)
    private Boolean euCustomer;
    @SerializedName(SERIALIZED_NAME_GROSS_PAYMENT)
    private Float grossPayment;
    @SerializedName(SERIALIZED_NAME_ID)
    private UUID id;
    @SerializedName(SERIALIZED_NAME_INTEGRATION_ID)
    private UUID integrationId;
    @SerializedName(SERIALIZED_NAME_INVOICE_NUMBER)
    private Integer invoiceNumber;
    @SerializedName(SERIALIZED_NAME_IS_DRAFT)
    private Boolean isDraft;
    @SerializedName(SERIALIZED_NAME_IS_LOCKED)
    private Boolean isLocked;
    @SerializedName(SERIALIZED_NAME_IS_OFFER)
    private Boolean isOffer;
    @SerializedName(SERIALIZED_NAME_ISSUED_DATE)
    private LocalDate issuedDate;
    @SerializedName(SERIALIZED_NAME_MESSAGE)
    private String message;
    @SerializedName(SERIALIZED_NAME_MODIFIED)
    private String modified;
    @SerializedName(SERIALIZED_NAME_NET_PAYMENT)
    private Float netPayment;
    @SerializedName(SERIALIZED_NAME_OFFER_NUMBER)
    private Integer offerNumber;
    @SerializedName(SERIALIZED_NAME_PAYMENT_DUE_DATE)
    private LocalDate paymentDueDate;
    @SerializedName(SERIALIZED_NAME_PAYMENT_TERM_ID)
    private UUID paymentTermId;
    @SerializedName(SERIALIZED_NAME_PROJECT_ID)
    private String project_id;
    @SerializedName(SERIALIZED_NAME_REFERENCE)
    private String reference;
    @SerializedName(SERIALIZED_NAME_VAT_PERCENT)
    private Integer vatPercent;


    public InvoiceDto companyId(UUID companyId) {

        this.companyId = companyId;
        return this;
    }

    /**
     * Get companyId
     *
     * @return companyId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getCompanyId() {
        return companyId;
    }


    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }


    public InvoiceDto contactId(UUID contactId) {

        this.contactId = contactId;
        return this;
    }

    /**
     * Get contactId
     *
     * @return contactId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getContactId() {
        return contactId;
    }


    public void setContactId(UUID contactId) {
        this.contactId = contactId;
    }


    public InvoiceDto created(String created) {

        this.created = created;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return created
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getCreated() {
        return created;
    }


    public void setCreated(String created) {
        this.created = created;
    }


    public InvoiceDto createdById(UUID createdById) {

        this.createdById = createdById;
        return this;
    }

    /**
     * Get createdById
     *
     * @return createdById
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getCreatedById() {
        return createdById;
    }


    public void setCreatedById(UUID createdById) {
        this.createdById = createdById;
    }


    public InvoiceDto currencyId(UUID currencyId) {

        this.currencyId = currencyId;
        return this;
    }

    /**
     * Get currencyId
     *
     * @return currencyId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getCurrencyId() {
        return currencyId;
    }


    public void setCurrencyId(UUID currencyId) {
        this.currencyId = currencyId;
    }


    public InvoiceDto dateFrom(LocalDate dateFrom) {

        this.dateFrom = dateFrom;
        return this;
    }

    /**
     * Get dateFrom
     *
     * @return dateFrom
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public LocalDate getDateFrom() {
        return dateFrom;
    }


    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }


    public InvoiceDto dateTo(LocalDate dateTo) {

        this.dateTo = dateTo;
        return this;
    }

    /**
     * Get dateTo
     *
     * @return dateTo
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public LocalDate getDateTo() {
        return dateTo;
    }


    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }


    public InvoiceDto deleted(String deleted) {

        this.deleted = deleted;
        return this;
    }

    /**
     * READ-ONLY - only present if it&#39;s an deleted object
     *
     * @return deleted
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

    public String getDeleted() {
        return deleted;
    }


    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }


    public InvoiceDto erpId(String erpId) {

        this.erpId = erpId;
        return this;
    }

    /**
     * Get erpId
     *
     * @return erpId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getErpId() {
        return erpId;
    }


    public void setErpId(String erpId) {
        this.erpId = erpId;
    }


    public InvoiceDto erpPaymentTermId(String erpPaymentTermId) {

        this.erpPaymentTermId = erpPaymentTermId;
        return this;
    }

    /**
     * Get erpPaymentTermId
     *
     * @return erpPaymentTermId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getErpPaymentTermId() {
        return erpPaymentTermId;
    }


    public void setErpPaymentTermId(String erpPaymentTermId) {
        this.erpPaymentTermId = erpPaymentTermId;
    }


    public InvoiceDto euCustomer(Boolean euCustomer) {

        this.euCustomer = euCustomer;
        return this;
    }

    /**
     * Get euCustomer
     *
     * @return euCustomer
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getEuCustomer() {
        return euCustomer;
    }


    public void setEuCustomer(Boolean euCustomer) {
        this.euCustomer = euCustomer;
    }


    public InvoiceDto grossPayment(Float grossPayment) {

        this.grossPayment = grossPayment;
        return this;
    }

    /**
     * Get grossPayment
     *
     * @return grossPayment
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Float getGrossPayment() {
        return grossPayment;
    }


    public void setGrossPayment(Float grossPayment) {
        this.grossPayment = grossPayment;
    }


    public InvoiceDto id(UUID id) {

        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getId() {
        return id;
    }


    public void setId(UUID id) {
        this.id = id;
    }


    public InvoiceDto integrationId(UUID integrationId) {

        this.integrationId = integrationId;
        return this;
    }

    /**
     * Get integrationId
     *
     * @return integrationId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getIntegrationId() {
        return integrationId;
    }


    public void setIntegrationId(UUID integrationId) {
        this.integrationId = integrationId;
    }


    public InvoiceDto invoiceNumber(Integer invoiceNumber) {

        this.invoiceNumber = invoiceNumber;
        return this;
    }

    /**
     * Get invoiceNumber
     *
     * @return invoiceNumber
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }


    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    public InvoiceDto isDraft(Boolean isDraft) {

        this.isDraft = isDraft;
        return this;
    }

    /**
     * Get isDraft
     *
     * @return isDraft
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIsDraft() {
        return isDraft;
    }


    public void setIsDraft(Boolean isDraft) {
        this.isDraft = isDraft;
    }


    public InvoiceDto isLocked(Boolean isLocked) {

        this.isLocked = isLocked;
        return this;
    }

    /**
     * Get isLocked
     *
     * @return isLocked
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIsLocked() {
        return isLocked;
    }


    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }


    public InvoiceDto isOffer(Boolean isOffer) {

        this.isOffer = isOffer;
        return this;
    }

    /**
     * Get isOffer
     *
     * @return isOffer
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Boolean getIsOffer() {
        return isOffer;
    }


    public void setIsOffer(Boolean isOffer) {
        this.isOffer = isOffer;
    }


    public InvoiceDto issuedDate(LocalDate issuedDate) {

        this.issuedDate = issuedDate;
        return this;
    }

    /**
     * Get issuedDate
     *
     * @return issuedDate
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public LocalDate getIssuedDate() {
        return issuedDate;
    }


    public void setIssuedDate(LocalDate issuedDate) {
        this.issuedDate = issuedDate;
    }


    public InvoiceDto message(String message) {

        this.message = message;
        return this;
    }

    /**
     * Get message
     *
     * @return message
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public InvoiceDto modified(String modified) {

        this.modified = modified;
        return this;
    }

    /**
     * READ-ONLY
     *
     * @return modified
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "READ-ONLY")

    public String getModified() {
        return modified;
    }


    public void setModified(String modified) {
        this.modified = modified;
    }


    public InvoiceDto netPayment(Float netPayment) {

        this.netPayment = netPayment;
        return this;
    }

    /**
     * Get netPayment
     *
     * @return netPayment
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Float getNetPayment() {
        return netPayment;
    }


    public void setNetPayment(Float netPayment) {
        this.netPayment = netPayment;
    }


    public InvoiceDto offerNumber(Integer offerNumber) {

        this.offerNumber = offerNumber;
        return this;
    }

    /**
     * Get offerNumber
     *
     * @return offerNumber
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Integer getOfferNumber() {
        return offerNumber;
    }


    public void setOfferNumber(Integer offerNumber) {
        this.offerNumber = offerNumber;
    }


    public InvoiceDto paymentDueDate(LocalDate paymentDueDate) {

        this.paymentDueDate = paymentDueDate;
        return this;
    }

    /**
     * Get paymentDueDate
     *
     * @return paymentDueDate
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public LocalDate getPaymentDueDate() {
        return paymentDueDate;
    }


    public void setPaymentDueDate(LocalDate paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }


    public InvoiceDto paymentTermId(UUID paymentTermId) {

        this.paymentTermId = paymentTermId;
        return this;
    }

    /**
     * Get paymentTermId
     *
     * @return paymentTermId
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public UUID getPaymentTermId() {
        return paymentTermId;
    }


    public void setPaymentTermId(UUID paymentTermId) {
        this.paymentTermId = paymentTermId;
    }


    public InvoiceDto project_id(String project_id) {

        this.project_id = project_id;
        return this;
    }

    /**
     * Get project_id
     *
     * @return project_id
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }


    public InvoiceDto reference(String reference) {

        this.reference = reference;
        return this;
    }

    /**
     * Get reference
     *
     * @return reference
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public String getReference() {
        return reference;
    }


    public void setReference(String reference) {
        this.reference = reference;
    }


    public InvoiceDto vatPercent(Integer vatPercent) {

        this.vatPercent = vatPercent;
        return this;
    }

    /**
     * Get vatPercent
     *
     * @return vatPercent
     **/
    @javax.annotation.Nullable
    @ApiModelProperty(value = "")

    public Integer getVatPercent() {
        return vatPercent;
    }


    public void setVatPercent(Integer vatPercent) {
        this.vatPercent = vatPercent;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InvoiceDto invoice = (InvoiceDto) o;
        return Objects.equals(this.companyId, invoice.companyId) &&
                Objects.equals(this.contactId, invoice.contactId) &&
                Objects.equals(this.created, invoice.created) &&
                Objects.equals(this.createdById, invoice.createdById) &&
                Objects.equals(this.currencyId, invoice.currencyId) &&
                Objects.equals(this.dateFrom, invoice.dateFrom) &&
                Objects.equals(this.dateTo, invoice.dateTo) &&
                Objects.equals(this.deleted, invoice.deleted) &&
                Objects.equals(this.erpId, invoice.erpId) &&
                Objects.equals(this.erpPaymentTermId, invoice.erpPaymentTermId) &&
                Objects.equals(this.euCustomer, invoice.euCustomer) &&
                Objects.equals(this.grossPayment, invoice.grossPayment) &&
                Objects.equals(this.id, invoice.id) &&
                Objects.equals(this.integrationId, invoice.integrationId) &&
                Objects.equals(this.invoiceNumber, invoice.invoiceNumber) &&
                Objects.equals(this.isDraft, invoice.isDraft) &&
                Objects.equals(this.isLocked, invoice.isLocked) &&
                Objects.equals(this.isOffer, invoice.isOffer) &&
                Objects.equals(this.issuedDate, invoice.issuedDate) &&
                Objects.equals(this.message, invoice.message) &&
                Objects.equals(this.modified, invoice.modified) &&
                Objects.equals(this.netPayment, invoice.netPayment) &&
                Objects.equals(this.offerNumber, invoice.offerNumber) &&
                Objects.equals(this.paymentDueDate, invoice.paymentDueDate) &&
                Objects.equals(this.paymentTermId, invoice.paymentTermId) &&
                Objects.equals(this.project_id, invoice.project_id) &&
                Objects.equals(this.reference, invoice.reference) &&
                Objects.equals(this.vatPercent, invoice.vatPercent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyId, contactId, created, createdById, currencyId, dateFrom, dateTo, deleted, erpId, erpPaymentTermId, euCustomer, grossPayment, id, integrationId, invoiceNumber, isDraft, isLocked, isOffer, issuedDate, message, modified, netPayment, offerNumber, paymentDueDate, paymentTermId, project_id, reference, vatPercent);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Invoice {\n");
        sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
        sb.append("    contactId: ").append(toIndentedString(contactId)).append("\n");
        sb.append("    created: ").append(toIndentedString(created)).append("\n");
        sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
        sb.append("    currencyId: ").append(toIndentedString(currencyId)).append("\n");
        sb.append("    dateFrom: ").append(toIndentedString(dateFrom)).append("\n");
        sb.append("    dateTo: ").append(toIndentedString(dateTo)).append("\n");
        sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
        sb.append("    erpId: ").append(toIndentedString(erpId)).append("\n");
        sb.append("    erpPaymentTermId: ").append(toIndentedString(erpPaymentTermId)).append("\n");
        sb.append("    euCustomer: ").append(toIndentedString(euCustomer)).append("\n");
        sb.append("    grossPayment: ").append(toIndentedString(grossPayment)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    integrationId: ").append(toIndentedString(integrationId)).append("\n");
        sb.append("    invoiceNumber: ").append(toIndentedString(invoiceNumber)).append("\n");
        sb.append("    isDraft: ").append(toIndentedString(isDraft)).append("\n");
        sb.append("    isLocked: ").append(toIndentedString(isLocked)).append("\n");
        sb.append("    isOffer: ").append(toIndentedString(isOffer)).append("\n");
        sb.append("    issuedDate: ").append(toIndentedString(issuedDate)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
        sb.append("    netPayment: ").append(toIndentedString(netPayment)).append("\n");
        sb.append("    offerNumber: ").append(toIndentedString(offerNumber)).append("\n");
        sb.append("    paymentDueDate: ").append(toIndentedString(paymentDueDate)).append("\n");
        sb.append("    paymentTermId: ").append(toIndentedString(paymentTermId)).append("\n");
        sb.append("    project_id: ").append(toIndentedString(project_id)).append("\n");
        sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
        sb.append("    vatPercent: ").append(toIndentedString(vatPercent)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

}

