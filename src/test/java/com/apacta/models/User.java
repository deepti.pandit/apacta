/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;

/**
 * User
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class User {

  public String company_id;
  public String city_id;
  public String country_id;
  public String language_id;
  public String created_by_id;
  public String contact_id;
  public String vendor_id;
  public String stock_location_id;
  public String time_entry_rule_group_id;
  public String first_name;
  public String last_name;
  public String initials;
  public String image;
  public String street_name;
  public String mobile_countrycode;
  public String cost_price;
  public String extra_price;
  public String sale_price;
  public String is_project_leader;
  public String is_active;
  public String is_view_only;
  public String erp_id;
  public String api_key;
  public String rfid_key;
  public String token_expires;
  public String receive_form_mails;
  public String hex_code;
  public String hide_address;
  public String hide_phone;
  public String full_name;
  public String total_cost_price;
  public String image_url;
  public String expected_billable_hours;

  public static final String SERIALIZED_NAME_API_KEY = "api_key";
  @SerializedName(SERIALIZED_NAME_API_KEY)
  private UUID apiKey;

  public static final String SERIALIZED_NAME_CITY_ID = "city_id";
  @SerializedName(SERIALIZED_NAME_CITY_ID)
  private UUID cityId;

  public static final String SERIALIZED_NAME_COMPANY_ID = "company_id";
  @SerializedName(SERIALIZED_NAME_COMPANY_ID)
  private UUID companyId;

  public static final String SERIALIZED_NAME_COST_PRICE = "cost_price";
  @SerializedName(SERIALIZED_NAME_COST_PRICE)
  private Float costPrice;

  public static final String SERIALIZED_NAME_CREATED = "created";
  @SerializedName(SERIALIZED_NAME_CREATED)
  private String created;

  public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
  @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
  private UUID createdById;

  public static final String SERIALIZED_NAME_DELETED = "deleted";
  @SerializedName(SERIALIZED_NAME_DELETED)
  private String deleted;

  public static final String SERIALIZED_NAME_EMAIL = "email";
  @SerializedName(SERIALIZED_NAME_EMAIL)
  private String email;

  public static final String SERIALIZED_NAME_EXTRA_PRICE = "extra_price";
  @SerializedName(SERIALIZED_NAME_EXTRA_PRICE)
  private Float extraPrice;

  public static final String SERIALIZED_NAME_FIRST_NAME = "first_name";
  @SerializedName(SERIALIZED_NAME_FIRST_NAME)
  private String firstName;

  public static final String SERIALIZED_NAME_FULL_NAME = "full_name";
  @SerializedName(SERIALIZED_NAME_FULL_NAME)
  private String fullName;

  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private UUID id;

  public static final String SERIALIZED_NAME_IS_ACTIVE = "is_active";
  @SerializedName(SERIALIZED_NAME_IS_ACTIVE)
  private Boolean isActive;

  public static final String SERIALIZED_NAME_LANGUAGE_ID = "language_id";
  @SerializedName(SERIALIZED_NAME_LANGUAGE_ID)
  private UUID languageId;

  public static final String SERIALIZED_NAME_LAST_NAME = "last_name";
  @SerializedName(SERIALIZED_NAME_LAST_NAME)
  private String lastName;

  public static final String SERIALIZED_NAME_MOBILE = "mobile";
  @SerializedName(SERIALIZED_NAME_MOBILE)
  private String mobile;

  public static final String SERIALIZED_NAME_MOBILE_COUNTRYCODE = "mobile_countrycode";
  @SerializedName(SERIALIZED_NAME_MOBILE_COUNTRYCODE)
  private String mobileCountrycode;

  public static final String SERIALIZED_NAME_MODIFIED = "modified";
  @SerializedName(SERIALIZED_NAME_MODIFIED)
  private String modified;

  public static final String SERIALIZED_NAME_PASSWORD = "password";
  @SerializedName(SERIALIZED_NAME_PASSWORD)
  private String password;

  public static final String SERIALIZED_NAME_PHONE = "phone";
  @SerializedName(SERIALIZED_NAME_PHONE)
  private String phone;

  public static final String SERIALIZED_NAME_PHONE_COUNTRYCODE = "phone_countrycode";
  @SerializedName(SERIALIZED_NAME_PHONE_COUNTRYCODE)
  private String phoneCountrycode;

  public static final String SERIALIZED_NAME_RECEIVE_FORM_MAILS = "receive_form_mails";
  @SerializedName(SERIALIZED_NAME_RECEIVE_FORM_MAILS)
  private Boolean receiveFormMails;

  public static final String SERIALIZED_NAME_SALE_PRICE = "sale_price";
  @SerializedName(SERIALIZED_NAME_SALE_PRICE)
  private Float salePrice;

  public static final String SERIALIZED_NAME_STREET_NAME = "street_name";
  @SerializedName(SERIALIZED_NAME_STREET_NAME)
  private String streetName;

  public static final String SERIALIZED_NAME_WEBSITE = "website";
  @SerializedName(SERIALIZED_NAME_WEBSITE)
  private String website;


  public User apiKey(UUID apiKey) {
    
    this.apiKey = apiKey;
    return this;
  }

   /**
   * Get apiKey
   * @return apiKey
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getApiKey() {
    return apiKey;
  }



  public void setApiKey(UUID apiKey) {
    this.apiKey = apiKey;
  }


  public User cityId(UUID cityId) {
    
    this.cityId = cityId;
    return this;
  }

   /**
   * Get cityId
   * @return cityId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getCityId() {
    return cityId;
  }



  public void setCityId(UUID cityId) {
    this.cityId = cityId;
  }


  public User companyId(UUID companyId) {
    
    this.companyId = companyId;
    return this;
  }

   /**
   * Get companyId
   * @return companyId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getCompanyId() {
    return companyId;
  }



  public void setCompanyId(UUID companyId) {
    this.companyId = companyId;
  }


  public User costPrice(Float costPrice) {
    
    this.costPrice = costPrice;
    return this;
  }

   /**
   * Cost of salaries
   * @return costPrice
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Cost of salaries")

  public Float getCostPrice() {
    return costPrice;
  }



  public void setCostPrice(Float costPrice) {
    this.costPrice = costPrice;
  }


  public User created(String created) {
    
    this.created = created;
    return this;
  }

   /**
   * READ-ONLY
   * @return created
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getCreated() {
    return created;
  }



  public void setCreated(String created) {
    this.created = created;
  }


  public User createdById(UUID createdById) {
    
    this.createdById = createdById;
    return this;
  }

   /**
   * Get createdById
   * @return createdById
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getCreatedById() {
    return createdById;
  }



  public void setCreatedById(UUID createdById) {
    this.createdById = createdById;
  }


  public User deleted(String deleted) {
    
    this.deleted = deleted;
    return this;
  }

   /**
   * READ-ONLY - only present if it&#39;s an deleted object
   * @return deleted
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

  public String getDeleted() {
    return deleted;
  }



  public void setDeleted(String deleted) {
    this.deleted = deleted;
  }


  public User email(String email) {
    
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getEmail() {
    return email;
  }



  public void setEmail(String email) {
    this.email = email;
  }


  public User extraPrice(Float extraPrice) {
    
    this.extraPrice = extraPrice;
    return this;
  }

   /**
   * Additional cost on this employee (pension, vacation etc.)
   * @return extraPrice
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "Additional cost on this employee (pension, vacation etc.)")

  public Float getExtraPrice() {
    return extraPrice;
  }



  public void setExtraPrice(Float extraPrice) {
    this.extraPrice = extraPrice;
  }


  public User firstName(String firstName) {
    
    this.firstName = firstName;
    return this;
  }

   /**
   * Get firstName
   * @return firstName
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getFirstName() {
    return firstName;
  }



  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public User fullName(String fullName) {
    
    this.fullName = fullName;
    return this;
  }

   /**
   * READ-ONLY
   * @return fullName
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getFullName() {
    return fullName;
  }



  public void setFullName(String fullName) {
    this.fullName = fullName;
  }


  public User id(UUID id) {
    
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getId() {
    return id;
  }



  public void setId(UUID id) {
    this.id = id;
  }


  public User isActive(Boolean isActive) {
    
    this.isActive = isActive;
    return this;
  }

   /**
   * Get isActive
   * @return isActive
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Boolean getIsActive() {
    return isActive;
  }



  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  public User languageId(UUID languageId) {
    
    this.languageId = languageId;
    return this;
  }

   /**
   * Get languageId
   * @return languageId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getLanguageId() {
    return languageId;
  }



  public void setLanguageId(UUID languageId) {
    this.languageId = languageId;
  }


  public User lastName(String lastName) {
    
    this.lastName = lastName;
    return this;
  }

   /**
   * Get lastName
   * @return lastName
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getLastName() {
    return lastName;
  }



  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  public User mobile(String mobile) {
    
    this.mobile = mobile;
    return this;
  }

   /**
   * Get mobile
   * @return mobile
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getMobile() {
    return mobile;
  }



  public void setMobile(String mobile) {
    this.mobile = mobile;
  }


  public User mobileCountrycode(String mobileCountrycode) {
    
    this.mobileCountrycode = mobileCountrycode;
    return this;
  }

   /**
   * Get mobileCountrycode
   * @return mobileCountrycode
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getMobileCountrycode() {
    return mobileCountrycode;
  }



  public void setMobileCountrycode(String mobileCountrycode) {
    this.mobileCountrycode = mobileCountrycode;
  }


  public User modified(String modified) {
    
    this.modified = modified;
    return this;
  }

   /**
   * READ-ONLY
   * @return modified
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getModified() {
    return modified;
  }



  public void setModified(String modified) {
    this.modified = modified;
  }


  public User password(String password) {
    
    this.password = password;
    return this;
  }

   /**
   * Get password
   * @return password
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPassword() {
    return password;
  }



  public void setPassword(String password) {
    this.password = password;
  }


  public User phone(String phone) {
    
    this.phone = phone;
    return this;
  }

   /**
   * Get phone
   * @return phone
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPhone() {
    return phone;
  }



  public void setPhone(String phone) {
    this.phone = phone;
  }


  public User phoneCountrycode(String phoneCountrycode) {
    
    this.phoneCountrycode = phoneCountrycode;
    return this;
  }

   /**
   * Get phoneCountrycode
   * @return phoneCountrycode
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getPhoneCountrycode() {
    return phoneCountrycode;
  }



  public void setPhoneCountrycode(String phoneCountrycode) {
    this.phoneCountrycode = phoneCountrycode;
  }


  public User receiveFormMails(Boolean receiveFormMails) {
    
    this.receiveFormMails = receiveFormMails;
    return this;
  }

   /**
   * If &#x60;true&#x60; the employee will receive an email receipt of every form submitted
   * @return receiveFormMails
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "If `true` the employee will receive an email receipt of every form submitted")

  public Boolean getReceiveFormMails() {
    return receiveFormMails;
  }



  public void setReceiveFormMails(Boolean receiveFormMails) {
    this.receiveFormMails = receiveFormMails;
  }


  public User salePrice(Float salePrice) {
    
    this.salePrice = salePrice;
    return this;
  }

   /**
   * The price this employee costs per hour when working
   * @return salePrice
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "The price this employee costs per hour when working")

  public Float getSalePrice() {
    return salePrice;
  }



  public void setSalePrice(Float salePrice) {
    this.salePrice = salePrice;
  }


  public User streetName(String streetName) {
    
    this.streetName = streetName;
    return this;
  }

   /**
   * Get streetName
   * @return streetName
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getStreetName() {
    return streetName;
  }



  public void setStreetName(String streetName) {
    this.streetName = streetName;
  }


  public User website(String website) {
    
    this.website = website;
    return this;
  }

   /**
   * Get website
   * @return website
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getWebsite() {
    return website;
  }



  public void setWebsite(String website) {
    this.website = website;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    User user = (User) o;
    return Objects.equals(this.apiKey, user.apiKey) &&
        Objects.equals(this.cityId, user.cityId) &&
        Objects.equals(this.companyId, user.companyId) &&
        Objects.equals(this.costPrice, user.costPrice) &&
        Objects.equals(this.created, user.created) &&
        Objects.equals(this.createdById, user.createdById) &&
        Objects.equals(this.deleted, user.deleted) &&
        Objects.equals(this.email, user.email) &&
        Objects.equals(this.extraPrice, user.extraPrice) &&
        Objects.equals(this.firstName, user.firstName) &&
        Objects.equals(this.fullName, user.fullName) &&
        Objects.equals(this.id, user.id) &&
        Objects.equals(this.isActive, user.isActive) &&
        Objects.equals(this.languageId, user.languageId) &&
        Objects.equals(this.lastName, user.lastName) &&
        Objects.equals(this.mobile, user.mobile) &&
        Objects.equals(this.mobileCountrycode, user.mobileCountrycode) &&
        Objects.equals(this.modified, user.modified) &&
        Objects.equals(this.password, user.password) &&
        Objects.equals(this.phone, user.phone) &&
        Objects.equals(this.phoneCountrycode, user.phoneCountrycode) &&
        Objects.equals(this.receiveFormMails, user.receiveFormMails) &&
        Objects.equals(this.salePrice, user.salePrice) &&
        Objects.equals(this.streetName, user.streetName) &&
        Objects.equals(this.website, user.website);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apiKey, cityId, companyId, costPrice, created, createdById, deleted, email, extraPrice, firstName, fullName, id, isActive, languageId, lastName, mobile, mobileCountrycode, modified, password, phone, phoneCountrycode, receiveFormMails, salePrice, streetName, website);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class User {\n");
    sb.append("    apiKey: ").append(toIndentedString(apiKey)).append("\n");
    sb.append("    cityId: ").append(toIndentedString(cityId)).append("\n");
    sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
    sb.append("    costPrice: ").append(toIndentedString(costPrice)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
    sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    extraPrice: ").append(toIndentedString(extraPrice)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("    languageId: ").append(toIndentedString(languageId)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    mobile: ").append(toIndentedString(mobile)).append("\n");
    sb.append("    mobileCountrycode: ").append(toIndentedString(mobileCountrycode)).append("\n");
    sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    phoneCountrycode: ").append(toIndentedString(phoneCountrycode)).append("\n");
    sb.append("    receiveFormMails: ").append(toIndentedString(receiveFormMails)).append("\n");
    sb.append("    salePrice: ").append(toIndentedString(salePrice)).append("\n");
    sb.append("    streetName: ").append(toIndentedString(streetName)).append("\n");
    sb.append("    website: ").append(toIndentedString(website)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

