/*
 * Apacta
 * API for a tool to craftsmen used to register working hours, material usage and quality assurance.     # Endpoint The endpoint `https://app.apacta.com/api/v1` should be used to communicate with the API. API access is only allowed with SSL encrypted connection (https). # Authentication URL query authentication with an API key is used, so appending `?api_key={api_key}` to the URL where `{api_key}` is found within Apacta settings is used for authentication # Pagination If the endpoint returns a `pagination` object it means the endpoint supports pagination - currently it's only possible to change pages with `?page={page_number}` but implementing custom page sizes are on the road map.   # Search/filter Is experimental but implemented in some cases - see the individual endpoints' docs for further explanation. # Ordering Is currently experimental, but on some endpoints it's implemented on URL querys so eg. to order Invoices by `invoice_number` appending `?sort=Invoices.invoice_number&direction=desc` would sort the list descending by the value of `invoice_number`. # Associations Is currently implemented on an experimental basis where you can append eg. `?include=Contacts,Projects`  to the `/api/v1/invoices/` endpoint to embed `Contact` and `Project` objects directly. # Project Files Currently project files can be retrieved from two endpoints. `/projects/{project_id}/files` handles files uploaded from wall posts or forms. `/projects/{project_id}/project_files` allows uploading and showing files, not belonging to specific form or wall post. # Errors/Exceptions ## 422 (Validation) Write something about how the `errors` object contains keys with the properties that failes validation like: ```   {       \"success\": false,       \"data\": {           \"code\": 422,           \"url\": \"/api/v1/contacts?api_key=5523be3b-30ef-425d-8203-04df7caaa93a\",           \"message\": \"A validation error occurred\",           \"errorCount\": 1,           \"errors\": {               \"contact_types\": [ ## Property name that failed validation                   \"Contacts must have at least one contact type\" ## Message with further explanation               ]           }       }   } ``` ## Code examples Running examples of how to retrieve the 5 most recent forms registered and embed the details of the User that made the form, and eventual products contained in the form ### Swift ```    ``` ### Java #### OkHttp ```   OkHttpClient client = new OkHttpClient();      Request request = new Request.Builder()     .url(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .get()     .addHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .addHeader(\"accept\", \"application/json\")     .build();      Response response = client.newCall(request).execute(); ``` #### Unirest ```   HttpResponse<String> response = Unirest.get(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")     .header(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\")     .header(\"accept\", \"application/json\")     .asString(); ``` ### Javascript #### Native ```   var data = null;      var xhr = new XMLHttpRequest();   xhr.withCredentials = true;      xhr.addEventListener(\"readystatechange\", function () {     if (this.readyState === 4) {       console.log(this.responseText);     }   });      xhr.open(\"GET\", \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   xhr.setRequestHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   xhr.setRequestHeader(\"accept\", \"application/json\");      xhr.send(data); ``` #### jQuery ```   var settings = {     \"async\": true,     \"crossDomain\": true,     \"url\": \"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\",     \"method\": \"GET\",     \"headers\": {       \"x-auth-token\": \"{INSERT_YOUR_TOKEN}\",       \"accept\": \"application/json\",     }   }      $.ajax(settings).done(function (response) {     console.log(response);   }); ``` #### NodeJS (Request) ```   var request = require(\"request\");    var options = { method: 'GET',     url: 'https://app.apacta.com/api/v1/forms',     qs:       { extended: 'true',        sort: 'Forms.created',        direction: 'DESC',        include: 'Products,CreatedBy',        limit: '5' },     headers:       { accept: 'application/json',        'x-auth-token': '{INSERT_YOUR_TOKEN}' } };      request(options, function (error, response, body) {     if (error) throw new Error(error);        console.log(body);   });  ``` ### Python 3 ```   import http.client      conn = http.client.HTTPSConnection(\"app.apacta.com\")      payload = \"\"      headers = {       'x-auth-token': \"{INSERT_YOUR_TOKEN}\",       'accept': \"application/json\",       }      conn.request(\"GET\", \"/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\", payload, headers)      res = conn.getresponse()   data = res.read()      print(data.decode(\"utf-8\")) ``` ### C# #### RestSharp ```   var client = new RestClient(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\");   var request = new RestRequest(Method.GET);   request.AddHeader(\"accept\", \"application/json\");   request.AddHeader(\"x-auth-token\", \"{INSERT_YOUR_TOKEN}\");   IRestResponse response = client.Execute(request);     ``` ### Ruby ```   require 'uri'   require 'net/http'      url = URI(\"https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5\")      http = Net::HTTP.new(url.host, url.port)   http.use_ssl = true   http.verify_mode = OpenSSL::SSL::VERIFY_NONE      request = Net::HTTP::Get.new(url)   request[\"x-auth-token\"] = '{INSERT_YOUR_TOKEN}'   request[\"accept\"] = 'application/json'      response = http.request(request)   puts response.read_body ``` ### PHP (HttpRequest) ```   <?php    $request = new HttpRequest();   $request->setUrl('https://app.apacta.com/api/v1/forms');   $request->setMethod(HTTP_METH_GET);      $request->setQueryData(array(     'extended' => 'true',     'sort' => 'Forms.created',     'direction' => 'DESC',     'include' => 'Products,CreatedBy',     'limit' => '5'   ));      $request->setHeaders(array(     'accept' => 'application/json',     'x-auth-token' => '{INSERT_YOUR_TOKEN}'   ));      try {     $response = $request->send();        echo $response->getBody();   } catch (HttpException $ex) {     echo $ex;   } ``` ### Shell (cURL) ```    $ curl --request GET --url 'https://app.apacta.com/api/v1/forms?extended=true&sort=Forms.created&direction=DESC&include=Products%2CCreatedBy&limit=5' --header 'accept: application/json' --header 'x-auth-token: {INSERT_YOUR_TOKEN}'                                    ```
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.apacta.models;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;

/**
 * VendorProduct
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-04-09T08:36:58.620878+02:00[Europe/Amsterdam]")
public class VendorProduct {
  public static final String SERIALIZED_NAME_BARCODE = "barcode";
  @SerializedName(SERIALIZED_NAME_BARCODE)
  private String barcode;

  public static final String SERIALIZED_NAME_CREATED = "created";
  @SerializedName(SERIALIZED_NAME_CREATED)
  private String created;

  public static final String SERIALIZED_NAME_CREATED_BY_ID = "created_by_id";
  @SerializedName(SERIALIZED_NAME_CREATED_BY_ID)
  private UUID createdById;

  public static final String SERIALIZED_NAME_DELETED = "deleted";
  @SerializedName(SERIALIZED_NAME_DELETED)
  private String deleted;

  public static final String SERIALIZED_NAME_DESCRIPTION = "description";
  @SerializedName(SERIALIZED_NAME_DESCRIPTION)
  private String description;

  public static final String SERIALIZED_NAME_ID = "id";
  @SerializedName(SERIALIZED_NAME_ID)
  private UUID id;

  public static final String SERIALIZED_NAME_MODIFIED = "modified";
  @SerializedName(SERIALIZED_NAME_MODIFIED)
  private String modified;

  public static final String SERIALIZED_NAME_NAME = "name";
  @SerializedName(SERIALIZED_NAME_NAME)
  private String name;

  public static final String SERIALIZED_NAME_PRICE = "price";
  @SerializedName(SERIALIZED_NAME_PRICE)
  private Double price;

  public static final String SERIALIZED_NAME_PRODUCT_CATEGORY_NUMBER = "product_category_number";
  @SerializedName(SERIALIZED_NAME_PRODUCT_CATEGORY_NUMBER)
  private String productCategoryNumber;

  public static final String SERIALIZED_NAME_PRODUCT_NUMBER = "product_number";
  @SerializedName(SERIALIZED_NAME_PRODUCT_NUMBER)
  private String productNumber;

  public static final String SERIALIZED_NAME_VENDOR_ID = "vendor_id";
  @SerializedName(SERIALIZED_NAME_VENDOR_ID)
  private UUID vendorId;


  public VendorProduct barcode(String barcode) {
    
    this.barcode = barcode;
    return this;
  }

   /**
   * Get barcode
   * @return barcode
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getBarcode() {
    return barcode;
  }



  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }


  public VendorProduct created(String created) {
    
    this.created = created;
    return this;
  }

   /**
   * READ-ONLY
   * @return created
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getCreated() {
    return created;
  }



  public void setCreated(String created) {
    this.created = created;
  }


  public VendorProduct createdById(UUID createdById) {
    
    this.createdById = createdById;
    return this;
  }

   /**
   * Get createdById
   * @return createdById
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getCreatedById() {
    return createdById;
  }



  public void setCreatedById(UUID createdById) {
    this.createdById = createdById;
  }


  public VendorProduct deleted(String deleted) {
    
    this.deleted = deleted;
    return this;
  }

   /**
   * READ-ONLY - only present if it&#39;s an deleted object
   * @return deleted
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY - only present if it's an deleted object")

  public String getDeleted() {
    return deleted;
  }



  public void setDeleted(String deleted) {
    this.deleted = deleted;
  }


  public VendorProduct description(String description) {
    
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getDescription() {
    return description;
  }



  public void setDescription(String description) {
    this.description = description;
  }


  public VendorProduct id(UUID id) {
    
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getId() {
    return id;
  }



  public void setId(UUID id) {
    this.id = id;
  }


  public VendorProduct modified(String modified) {
    
    this.modified = modified;
    return this;
  }

   /**
   * READ-ONLY
   * @return modified
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "READ-ONLY")

  public String getModified() {
    return modified;
  }



  public void setModified(String modified) {
    this.modified = modified;
  }


  public VendorProduct name(String name) {
    
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getName() {
    return name;
  }



  public void setName(String name) {
    this.name = name;
  }


  public VendorProduct price(Double price) {
    
    this.price = price;
    return this;
  }

   /**
   * Get price
   * @return price
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Double getPrice() {
    return price;
  }



  public void setPrice(Double price) {
    this.price = price;
  }


  public VendorProduct productCategoryNumber(String productCategoryNumber) {
    
    this.productCategoryNumber = productCategoryNumber;
    return this;
  }

   /**
   * Get productCategoryNumber
   * @return productCategoryNumber
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getProductCategoryNumber() {
    return productCategoryNumber;
  }



  public void setProductCategoryNumber(String productCategoryNumber) {
    this.productCategoryNumber = productCategoryNumber;
  }


  public VendorProduct productNumber(String productNumber) {
    
    this.productNumber = productNumber;
    return this;
  }

   /**
   * Get productNumber
   * @return productNumber
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getProductNumber() {
    return productNumber;
  }



  public void setProductNumber(String productNumber) {
    this.productNumber = productNumber;
  }


  public VendorProduct vendorId(UUID vendorId) {
    
    this.vendorId = vendorId;
    return this;
  }

   /**
   * Get vendorId
   * @return vendorId
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public UUID getVendorId() {
    return vendorId;
  }



  public void setVendorId(UUID vendorId) {
    this.vendorId = vendorId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VendorProduct vendorProduct = (VendorProduct) o;
    return Objects.equals(this.barcode, vendorProduct.barcode) &&
        Objects.equals(this.created, vendorProduct.created) &&
        Objects.equals(this.createdById, vendorProduct.createdById) &&
        Objects.equals(this.deleted, vendorProduct.deleted) &&
        Objects.equals(this.description, vendorProduct.description) &&
        Objects.equals(this.id, vendorProduct.id) &&
        Objects.equals(this.modified, vendorProduct.modified) &&
        Objects.equals(this.name, vendorProduct.name) &&
        Objects.equals(this.price, vendorProduct.price) &&
        Objects.equals(this.productCategoryNumber, vendorProduct.productCategoryNumber) &&
        Objects.equals(this.productNumber, vendorProduct.productNumber) &&
        Objects.equals(this.vendorId, vendorProduct.vendorId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(barcode, created, createdById, deleted, description, id, modified, name, price, productCategoryNumber, productNumber, vendorId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VendorProduct {\n");
    sb.append("    barcode: ").append(toIndentedString(barcode)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    createdById: ").append(toIndentedString(createdById)).append("\n");
    sb.append("    deleted: ").append(toIndentedString(deleted)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    productCategoryNumber: ").append(toIndentedString(productCategoryNumber)).append("\n");
    sb.append("    productNumber: ").append(toIndentedString(productNumber)).append("\n");
    sb.append("    vendorId: ").append(toIndentedString(vendorId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

