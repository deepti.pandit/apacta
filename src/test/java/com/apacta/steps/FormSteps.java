package com.apacta.steps;

import com.apacta.actions.FormActions;
import com.apacta.models.Form;
import com.apacta.models.FormDto;
import com.apacta.models.Forms;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FormSteps {

    @Steps
    FormActions formActions = new FormActions();

    @When("the user send a GET request for form")
    public void theUserSendAGETRequestForForm() throws Exception {
        formActions.requestFormWithGetMethod();
    }

    @Then("form request should have status code as {int}")
    public void formGetRequestShouldHaveStatusCodeAs(int statusCode) throws Exception {
        assertEquals("Status Code is not as expected", statusCode, formActions.getStatusCode());
    }

    @And("Verify the fields in GET form response$")
    public void verifyTheFieldsInGETFormResponse() throws Exception {
        Forms forms = formActions.getBody();
        assertNotNull(forms.success);
        assertNotNull(forms.getForms());
        FormDto formDto = forms.getForms().get(0);
        assertNotNull(formDto.getId());
        assertNotNull(formDto.getCompany_id());
    }

    @When("the user send a GET request by providing form id (.*)$")
    public void theUserSendAGETRequestByProvidingFormIdForm_id(String form_id) throws Exception {
        formActions.requestFormByProvidingFormIdWithGetMethod(form_id);
    }

    @And("Verify the fields in GET form with Form id response$")
    public void verifyTheFieldsInGETFormResponseWithFormId() throws Exception {
        Form form = formActions.getFormBody();
        assertNotNull(form.success);
    }
}
