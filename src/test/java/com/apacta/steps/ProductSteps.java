package com.apacta.steps;

import com.apacta.actions.ProductActions;
import com.apacta.models.Product;
import com.apacta.models.ProductDto;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProductSteps {

    @Steps
    ProductActions productActions = new ProductActions();

    @When("the user send a GET request for product")
    public void theUserSendAGETRequestForProduct() throws Exception {
        productActions.requestProductWithGetMethod();
    }

    @Then("product get request should have status code as {int}")
    public void productGetRequestShouldHaveStatusCodeAs(int statusCode) throws Exception {
        assertEquals("Status Code is not as expected", statusCode, productActions.getStatusCode());
    }

    @And("Verify the fields in GET product response$")
    public void verifyTheFieldsInGETProductResponse() throws Exception {
        Product product = productActions.getBody();
        assertNotNull(product.success);
        assertNotNull(product.getProducts());
        ProductDto productDto = product.getProducts().get(0);
        assertNotNull(productDto.getId());
        assertNotNull(productDto.getCompany_id());
    }
}
