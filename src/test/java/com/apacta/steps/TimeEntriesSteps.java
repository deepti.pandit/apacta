package com.apacta.steps;

import com.apacta.actions.TimeEntriesActions;
import com.apacta.models.TimeEntry;
import com.apacta.models.TimeEntryDto;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TimeEntriesSteps {

    @Steps
    TimeEntriesActions timeEntriesActions = new TimeEntriesActions();

    @When("the user send a GET request of time entries")
    public void theUserSendAGETRequestOfTimeEntries() throws Exception {
        timeEntriesActions.requestTimeEntriesWithGetMethod();
    }

    @Then("request should have status code as {int}")
    public void requestShouldHaveStatusCodeAs(int statusCode) throws Exception {
        assertEquals("Status Code is not as expected", statusCode, timeEntriesActions.getStatusCode());
    }

    @And("Verify the time entries fields in GET time entries response$")
    public void verifyTheFieldsInGETMarketResponse() throws Exception {
        TimeEntry timeEntry = timeEntriesActions.getBody();
        assertNotNull(timeEntry.success);
        assertNotNull(timeEntry.getTimeEntries());
        TimeEntryDto timeEntryDto = timeEntry.getTimeEntries().get(0);
        assertNotNull(timeEntryDto.getId());
        assertNotNull(timeEntryDto.getProject_id());
    }

    @When("user send POST request with settings form_id (.*), from_time (.*), is_all_day (.*), project_id (.*), sum (.*), time_entry_type_id (.*), to_time (.*) and user_id (.*)$")
    public void userSendPOSTRequestWithSettings(String form_id, String from_time, String is_all_day, String project_id, String sum, String time_entry_type_id, String to_time, String user_id) throws Exception {
        timeEntriesActions.requestSentForCreateInvoiceWithPostMethod(form_id, from_time, is_all_day, project_id, sum, time_entry_type_id, to_time, user_id);

    }
}
