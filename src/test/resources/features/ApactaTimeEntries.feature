Feature: Time Entries management

  @GetTimeEntriesGET-200
  Scenario: Get time entries
    When the user send a GET request of time entries
    Then request should have status code as 200
    And Verify the time entries fields in GET time entries response

  @CreateTimeEntriesPost-200
  Scenario Outline: POST time entries
    When user send POST request with settings form_id <form_id>, from_time <from_time>, is_all_day <is_all_day>, project_id <project_id>, sum <sum>, time_entry_type_id <time_entry_type_id>, to_time <to_time> and user_id <user_id>
    Then request should have status code as 200
    Examples:
      | form_id                              | from_time | is_all_day | project_id                           | sum   | time_entry_type_id | to_time | user_id                              |
      | 610fd76b-1ae2-4784-97d0-e3b92e5bfd26 |           | true       | 0025a01c-c176-4308-87d7-e19f457ba003 | 31500 | Integer            |         | 29391f53-c139-4b3a-a1a0-c86d39e29c2e |

  @FailedCreateTimeEntriesPost-404
  Scenario Outline: Failed POST time entries
    When user send POST request with settings form_id <form_id>, from_time <from_time>, is_all_day <is_all_day>, project_id <project_id>, sum <sum>, time_entry_type_id <time_entry_type_id>, to_time <to_time> and user_id <user_id>
    Then request should have status code as 404
    Examples:
      | form_id                              | from_time | is_all_day | project_id                           | sum   | time_entry_type_id | to_time | user_id                              |
      | 610fd76b-1ae2-4784-97d0-e3b92e5bfd26 |           | true       | 0025a01c-c176-4308-87d7-e19f457ba003 | 31500 | Integer            |         | 29391f53-c139-4b3a-a1a0-c86d39e29c23 |
