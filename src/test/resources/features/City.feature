Feature: City management

  @GetCityGET-200
  Scenario: Get City
    When the user send a GET request for city
    Then city get request should have status code as 200
    And Verify the fields in GET city response