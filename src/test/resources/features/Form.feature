Feature: Form management

  @GetProductGET-200
  Scenario: Get forms
    When the user send a GET request for form
    Then form request should have status code as 200
    And Verify the fields in GET form response

  @GetProductGET-200
  Scenario Outline: Get form with form id
    When the user send a GET request by providing form id <form_id>
    Then form request should have status code as 200
    And Verify the fields in GET form with Form id response
    Examples:
      | form_id                              |
      | 442e2795-4d2a-4764-a796-9037195aef05 |